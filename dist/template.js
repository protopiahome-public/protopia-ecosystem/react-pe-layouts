import Layouts from "react-pe-layouts";
export function cssStyle() {
  const css = localStorage.getItem("css");
  const style = css && css !== "undefined" ? css : Layouts().template.style;
  return style;
}
export function styles() {
  // console.log(Layouts());
  return Layouts().template.styles;
}
export function currentStyles() {
  return Layouts().template.style;
}
export function byId(id) {
  const st = styles().filter(e => e._id == id)[0];
  return st || {};
}
export function isMenuLeft() {
  // console.log(Layouts().template.menu_left)
  if (Layouts().template) {
    return Layouts().template.menu_left || 0;
  }
}
export function login() {
  return Layouts().template.login;
}
export function template() {
  if (Layouts().template) return Layouts().template;
}
export function widgets() {
  if (Layouts().widgets) return Layouts().widgets;
}
export function areas() {
  const area = Layouts()["widget-area"] ? Layouts()["widget-area"] : {};
  const plgns = Layouts().modules;
  Object.keys(plgns).forEach(plugin => {
    Object.keys(plgns[plugin].extentions).forEach(add_on => {
      plgns[plugin].extentions[add_on].area.forEach(ar => {
        // console.log( area )
        // const areaArea = area[ ar.id ] ? area[ ar.id ].area : []
        area[ar.id] = {
          title: plgns[plugin].extentions[add_on].title,
          description: plgns[plugin].extentions[add_on].description,
          area: [// ...areaArea,
          {
            component: add_on
          }]
        };
      }); //
    });
  }); // console.log( area )

  return area;
}
export function avatar() {
  if (Layouts().template && Layouts().template.avatar) return Layouts().template.avatar;
}
export function loginPage() {
  return Layouts().template.login;
}
export function iconUrl() {
  return `url(${Layouts().template.icon})`;
}
export function iconHeight() {
  return Layouts().template.icon_height ? Layouts().template.icon_height : 30;
}
export function iconWidth() {
  return Layouts().template.icon_width ? Layouts().template.icon_width : 30;
}
export function getAllVisibleValue(object, visile_value_srting) {
  const arr = visile_value_srting ? visile_value_srting.split(".") : [];
  if (!object) return {};
  let adr = { ...object
  };

  for (let i = 0; i < arr.length; i++) {
    adr = adr[arr[i]];
  }

  return adr;
}