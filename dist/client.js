import { ApolloClient } from "apollo-client";
import { ApolloLink } from "apollo-link";
import { RestLink } from "apollo-link-rest";
import { HttpLink } from "apollo-link-http";
import { setContext } from "apollo-link-context";
import { onError } from "apollo-link-error";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createUploadLink } from "apollo-upload-client";
import { __ } from "react-pe-utilities";
export function client(server_url = "") {
  const restLink = new RestLink({
    uri: server_url ? server_url : null
  });
  const httpLink = new HttpLink({
    uri: server_url ? server_url : null
  });
  const uploadLink = createUploadLink({
    uri: server_url ? server_url : null
  });
  const queryLink = uploadLink; // const queryLink = (link_type() === "http") ? httpLink: restLink;

  const cache = new InMemoryCache();
  const authLink = setContext((_, {
    headers
  }) => {
    let new_headers;

    if (localStorage.getItem("token")) {
      const token = localStorage.getItem("token") ? localStorage.getItem("token") : "";
      new_headers = { ...headers,
        Authorization: `Bearer ${token}`
      };
    } else if (localStorage.getItem("client_token")) {
      const token = localStorage.getItem("client_token") ? localStorage.getItem("client_token") : "";
      new_headers = { ...headers,
        Authorization: `Bearer ${token}`
      };
    } else {
      new_headers = { ...headers
      };
    } // "authorization": 'Bearer ' + token,
    // return the headers to the context so httpLink can read them
    // const xxx = base64_encode( login +':'+ password );
    // console.log(new_headers);


    return {
      headers: new_headers
    };
  });
  const errorlink = onError(({
    graphQLErrors,
    networkError,
    operation,
    forward
  }) => {
    if (graphQLErrors) {
      for (const err of graphQLErrors) {
        if (err.extensions) {
          switch (err.extensions.code) {
            case "INTERNAL_SERVER_ERROR":
              console.log(err);
              break;

            case "FORBIDDEN":
              console.log(err);
              break;

            default:
              alert(__(err.message));
              break;
          }
        } else {
          alert(__(err.message));
        }
      }
    }
  });
  const defaultOptions = {
    watchQuery: {
      fetchPolicy: "network-only" // errorPolicy: 'ignore',

    },
    query: {
      fetchPolicy: "network-only" // errorPolicy: 'all',

    }
  };
  return new ApolloClient({
    link: ApolloLink.from([errorlink, authLink, queryLink]),
    cache,
    defaultHttpLink: false,
    defaultOptions,
    fetchOptions: {
      credentials: "include",
      mode: "no-cors"
    }
  });
}