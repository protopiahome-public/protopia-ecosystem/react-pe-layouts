
import Layouts from "./layouts"

export function title() {
  if (Layouts().app && Layouts().app.title) return Layouts().app.title
}

export function name() {
  if (Layouts().app && Layouts().app.name) return Layouts().app.name
}
export function description() {
  if (Layouts().app && Layouts().app.description) return Layouts().app.description
}
export function help_url()
{
  if (Layouts().app && Layouts().app.help_url) return Layouts().app.help_url
}

export function externalSystems() {
  return Layouts().app.external_systems
}
export function roles() {
  return Layouts().app.roles
}
export function adapter() {
  return Layouts().app.adapter
}
export function userModel() {
  return Layouts().app.user_model
}
//export default Layouts().app;
